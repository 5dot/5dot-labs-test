const http = require('http');
const fs = require('fs');

const server = http.createServer((req, res) => {

  req.on('error', err => {
      console.error(err);
      // Handle error...
      res.statusCode = 400;
      res.end('400: Bad Request');
      return;
  });

  res.on('error', err => {
      console.error(err);
      // Handle error...
  });

  fs.readFile('./public' + req.url, (err, data) => {
    if (err) {
        if (req.url === '/' && req.method === 'GET') {
            res.end('Welcome Home');
        } else if (req.url === '/local-ip' && req.method === 'GET') {
          var data = getLocalIps();
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(data));
          } else if (req.url === '/device-info' && req.method === 'GET') {
          var data = getDeviceInfo();
          res.setHeader('Content-Type', 'application/json');
          res.end(JSON.stringify(data));
        } else if (req.url === '/reboot') {
                res.end('shutdown')
        require('child_process').exec('sudo /sbin/shutdown -r now', function (msg) { console.log(msg) });

        } else {
            res.statusCode = 404;
            res.end('404: File Not Found');
        }
    } else {
        // NOTE: The file name could be parsed to determine the
        // appropriate data type to return. This is just a quick
        // example.
        res.setHeader('Content-Type', 'application/octet-stream');
        res.end(data);
    }
  });

});

server.listen(8080, () => {
    console.log('Example app listening on port 8080!');
});




var getLocalIps = function(){

  var os = require('os');
  var ifaces = os.networkInterfaces();
  var returnValue = [];

  Object.keys(ifaces).forEach(function (ifname) {
    var alias = 0;

    ifaces[ifname].forEach(function (iface) {
      if ('IPv4' !== iface.family || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return;
      }

      if (alias >= 1) {
        // this single interface has multiple ipv4 addresses
        console.log(ifname + ':' + alias, iface.address);
        returnValue.push({
          ifname: ifname
          , alias: alias
          , address: iface.address
        });
      } else {
        // this interface has only one ipv4 adress
        console.log(ifname, iface.address);
        returnValue.push({
          ifname: ifname
          , address: iface.address
        });
      }
      ++alias;
    });

  });
  return returnValue;
}






var GLOBAL_SOFTWARE_VERSION_NUMBER = 1;

var getDeviceInfo = function(){
  var returnValue = {};

  var os = require('os');

  returnValue.version = GLOBAL_SOFTWARE_VERSION_NUMBER;
  returnValue.arch = os.arch();
  returnValue.cpu = os.cpus()[0].model;
  returnValue.type = os.type();
  returnValue.platform = os.platform();
  returnValue.hostname = os.hostname();
  returnValue.release = os.release();

  returnValue.networkInterfaces = [];

  var ifaces = os.networkInterfaces();
  Object.keys(ifaces).forEach(function (ifname) {
    var alias = 0;

    ifaces[ifname].forEach(function (iface) {
      if ('IPv4' !== iface.family || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return;
      }

      if (alias >= 1) {
        // this single interface has multiple ipv4 addresses
        //console.log(ifname + ':' + alias, iface.address);
        returnValue.networkInterfaces.push({
          ifname: ifname
          , alias: alias
          , address: iface.address
        });
      } else {
        // this interface has only one ipv4 adress
        //console.log(ifname, iface.address);
        returnValue.networkInterfaces.push({
          ifname: ifname
          , address: iface.address
        });
      }
      ++alias;
    });

  });


  return returnValue;
}
